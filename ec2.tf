data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

module "bastion" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "1.21.0"
  name = "bastion"
  instance_count = 1
  tags = "${local.resource_tags}"
  ami = "${data.aws_ami.amazon_linux.id}"
  instance_type = "t2.micro"
  key_name = "${module.ssh_keypair.name}"
  monitoring = false
  vpc_security_group_ids = ["${module.bastion_ssh_sg.this_security_group_id}"]
  subnet_id = "${join(",", random_shuffle.single_public_subnet_id.result)}"
  associate_public_ip_address = true
  root_block_device = [{
    volume_type = "gp2"
    volume_size = 10
  }]
}

module "webserver" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "1.21.0"
  name = "webserver"
  instance_count = 1
  tags = "${local.resource_tags}"
  ami = "${data.aws_ami.amazon_linux.id}"
  instance_type = "t2.micro"
  key_name = "${module.ssh_keypair.name}"
  monitoring = false
  vpc_security_group_ids = ["${module.private_instance_sg.this_security_group_id}"]
  subnet_id = "${join(",", random_shuffle.single_private_subnet_id.result)}"
  associate_public_ip_address = false
  root_block_device = [{
    volume_type = "gp2"
    volume_size = 10
  }]
}
