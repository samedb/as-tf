module "ssh_keypair" {
  source = "github.com/hashicorp-modules/ssh-keypair-aws"
  name = "poc_key"
  rsa_bits = 4096
}
