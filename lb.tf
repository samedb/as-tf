module "alb" {
  source = "terraform-aws-modules/alb/aws"
  load_balancer_name = "poc-alb"
  security_groups = ["${module.lb_http_sg.this_security_group_id}"]
  subnets = ["${module.vpc.public_subnets}"]
  tags = "${map("Environment", "test")}"
  vpc_id = "${module.vpc.vpc_id}"
  https_listeners = "${list(map("certificate_arn", "${aws_iam_server_certificate.ssl_cert.arn}", "port", 443))}"
  https_listeners_count = "1"
  http_tcp_listeners = "${list(map("port", "80", "protocol", "HTTP"))}"
  http_tcp_listeners_count = "1"
  target_groups = "${list(map("name", "${var.web_target_group}", "backend_protocol", "${var.web_target_protocol}", "backend_port", "${var.web_target_port}"))}"
  target_groups_count = "1"
  logging_enabled = false
}
