output "lb_url" {
   description = "URL of load balancer"
    value = "${module.alb.dns_name}"
}
