resource "random_shuffle" "single_resource_az" {
  input = ["${var.availability_zones}"]
  result_count = 1
}

resource "random_shuffle" "single_private_subnet_id" {
  input = ["${module.vpc.private_subnets}"]
  result_count = 1
}

resource "random_shuffle" "single_public_subnet_id" {
  input = ["${module.vpc.public_subnets}"]
  result_count = 1
}

resource "aws_iam_server_certificate" "ssl_cert" {
  name_prefix      = "poc_cert"
  certificate_body = "${file("private/cert.pem")}"
  private_key      = "${file("private/key.pem")}"

  lifecycle {
    create_before_destroy = true
  }
}
resource "null_resource" "dummy_dependency" {
  depends_on = ["module.vpc"]
}

output "depends_id" {
  value = "${null_resource.dummy_dependency.id}"
}

resource "null_resource" "provision_webserver" {
 depends_on = ["null_resource.dummy_dependency"]
  provisioner "file" {
    source      = "provisioner.sh"
    destination = "/tmp/provisioner.sh"
  }
   provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/provisioner.sh",
      "sudo /tmp/provisioner.sh"
    ]
  }
    connection {
      type     = "ssh"
      user     = "ec2-user"
      host     = "${module.webserver.private_ip}"
      private_key = "${module.ssh_keypair.private_key_pem}"
      bastion_host  = "${module.bastion.public_ip}"
    }
}


resource "aws_lb_target_group_attachment" "webserver" {
  target_group_arn = "${join(",", module.alb.target_group_arns)}"
  target_id        = "${join(",", module.webserver.id)}"
  port             = 80
}
