module "bastion_ssh_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/ssh"

  name        = "bastion_sg"
  description = "Security group for bastion host resides in public subnet"
  vpc_id      = "${module.vpc.vpc_id}"
  
  auto_ingress_with_self = []
  ingress_cidr_blocks = ["${var.whitelisted_ssh_ips}"]
  ingress_ipv6_cidr_blocks = []
  auto_egress_rules = []
  egress_rules = ["ssh-tcp"]
  egress_cidr_blocks = ["${var.private_subnets_cidrs}","${var.public_subnets_cidrs}"]
  egress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "HTTP access"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "HTTPS access"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  egress_ipv6_cidr_blocks = []
  tags = "${local.resource_tags}"
}

module "private_instance_sg" {
  source = "terraform-aws-modules/security-group/aws"
  
  name        = "private_instance_sg"
  description = "Security group for instance resides in private subnet"
  vpc_id      = "${module.vpc.vpc_id}"
  ingress_cidr_blocks = ["${var.public_subnets_cidrs}"]
  ingress_rules = ["https-443-tcp","http-80-tcp","ssh-tcp"]
  egress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules = ["all-all"]
  tags = "${local.resource_tags}"
}


module "lb_http_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "lb_http_sg"
  description = "Security group for lb resides in public subnet"
  vpc_id      = "${module.vpc.vpc_id}"
  
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules = ["http-80-tcp","https-443-tcp"]
  egress_rules = ["http-80-tcp", "https-443-tcp"]
  egress_cidr_blocks = ["${var.private_subnets_cidrs}"]
  egress_ipv6_cidr_blocks = []
  tags = "${local.resource_tags}"
}

