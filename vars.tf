variable "vpc_name" {
  default = "poc-vpc"
}

variable "vpc_cidr" {
  default = "172.16.0.0/16"
}

variable "availability_zones" {
  default = ["eu-west-1a", "eu-west-1b"]
}

variable "private_subnets_cidrs" {
  default = ["172.16.2.0/24","172.16.3.0/24"]
}
   
variable "public_subnets_cidrs" {
  default = ["172.16.0.0/24", "172.16.1.0/24"]
}

variable "whitelisted_ssh_ips" {
  default = [
    "46.196.101.160/32",
    "195.212.230.21/32",
    "195.201.233.53/32"
  ]
}


variable "web_target_group" {
  default = "webserver"
}

variable "web_target_protocol" {
  default = "HTTP"
}

variable "web_target_port" {
  default = "80"
}

variable depends_on { default = [], type = "list"}
