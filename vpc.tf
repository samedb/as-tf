module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "1.64.0"
  name = "${var.vpc_name}"
  cidr = "${var.vpc_cidr}"

  azs             = ["${var.availability_zones}"]
  private_subnets = ["${var.private_subnets_cidrs}"]
  public_subnets  = ["${var.public_subnets_cidrs}"]

  enable_nat_gateway = true
  single_nat_gateway = false
  one_nat_gateway_per_az = true

  tags = "${local.resource_tags}"
}

